import { getUserById } from "../../../model/user";

const userController = {
  async getUser(req, res) {
    res.status(200).json(getUserById(parseInt(req.params.id)));
  }
};
export default userController;
