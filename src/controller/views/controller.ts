import * as express from "express";

const pages = {
  async getHome(req: express.Request, res: express.Response) {
    res.render("index", { error: false });
  },
  async getAbout(req: express.Request, res: express.Response) {
    res.render("user", { error: false });
  }
};
export default pages;
