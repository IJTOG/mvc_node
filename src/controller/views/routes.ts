import controllers from "./controller";
export function setup(router) {
  router.get("/", controllers.getHome);
  router.get("/about", controllers.getAbout);
}
