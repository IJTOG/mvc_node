let user = [
  {
    id: 1,
    name: "ratta",
    age: 20,
    saraly: 10000
  },
  {
    id: 2,
    name: "yani",
    age: 19,
    saraly: 17000
  },
  {
    id: 3,
    name: "pusith",
    age: 23,
    saraly: 12000
  },
  {
    id: 4,
    name: "wandee",
    age: 22,
    saraly: 20000
  },
  {
    id: 5,
    name: "natta",
    age: 21,
    saraly: 15000
  }
];

const getUserById = (id: number) => {
  return user.find(item => {
    return item.id === id;
  });
};

export { getUserById };
