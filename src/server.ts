import dotenv from "dotenv";
import express from "express";
import path from "path";
import fs from "fs";

// initialize configuration
dotenv.config();

// port is now available to the Node.js runtime
// as if it were an environment variable
const port = process.env.SERVER_PORT || 3000;

const setupApiRoutes = (App: express.Application) => {
  const APP_DIR = `${__dirname}\\controller\\api`;
  const features = fs
    .readdirSync(APP_DIR)
    .filter(file => fs.statSync(`${APP_DIR}\\${file}`).isDirectory());
  features.forEach(feature => {
    const router = express.Router();
    const routes = require(`${APP_DIR}\\${feature}\\routes`);
    routes.setup(router);
    App.use(`/api/${feature}`, router);
  });
};

// define a route handler for the default home page
const setupViewRoutes = (App: express.Application) => {
  const APP_DIR = `${__dirname}\\controller\\views`;
  const router = express.Router();
  const routes = require(`${APP_DIR}\\routes`);
  routes.setup(router);
  App.use(`/`, router);
};

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
setupApiRoutes(app);
setupViewRoutes(app);

// start the Express server
app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`);
});
